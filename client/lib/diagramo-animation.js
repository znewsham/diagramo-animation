DIAGRAMO.FPS = 33.3333333333333;
DIAGRAMO.Animation = {};
DIAGRAMO.frames = {};
DIAGRAMO.Animation.offset = 0;
DIAGRAMO.Animation.init = function(diagramo){
  _.extend(diagramo, {
    setFrame: function(index){
      this.skipTextSelect = true;
      var self = this;
      var scale = this.scale;
      if(this.editable){
        this.paintables.forEach(function(p){
          if(p.isGroup){
            self.destroyGroup(p);
            self.removePaintable(p);
          }
        });
        this.frames[this.currentIndex] = this.clone();
      }
      var frame = this.frames[index];
      this.load(frame, true);
      this.setScale(scale);
      this.currentIndex = index;
      this.skipTextSelect = false;
    },
    deleteFrame: function(index){
      if(index == 0){//can't delete the origin frame
        return;
      }
      if(index == this.currentIndex){
        this.setFrame(index - 1);
      }
      this.frames.splice(index, this.frames.length - index);
      this.updateThumbNails();
    },
    updateThumbNails: function(){
      var self = this;
      var thumbnails = $(this.thumbnails);
      $("#thumbnails div").remove();
      var tmpCanvas = document.createElement("canvas");
      tmpCanvas.width = this.canvas.width;
      tmpCanvas.height = this.canvas.height;


      var scale = self.scale;
      tmpCanvas.getContext("2d").scale(scale, scale);
      this.frames[this.currentIndex] = this.clone();
      this.frames.forEach(function(frame, index){
        self.load(frame, true);
        self.setScale(scale);
        self.paint(tmpCanvas.getContext("2d"));
        var img = $("<img>");
        img[0].src = tmpCanvas.toDataURL("image/png");
        img.width(140);
        img.addClass("thumbnail");
        img[0].id = "thumbnails-" + index;

        var span = $("<div>");
        span.append(img);
        if(index > 0){
          var del = $("<i>");
          del.addClass("fa fa-trash")
          del.css("z-index", 100);
          del[0].id = "delete-" + index;
          span.append(del);
        }
        thumbnails.append(span);
        if(index == self.currentIndex){
          img.addClass("current");
        }
        if(index > 0){
          del.click(function(e){
            var index = parseInt(e.target.id.replace("delete-",""));
            var confirmed = true;
            if(index < self.frames.length - 1){
              confirmed = confirm("this will delete multiple frames, continue?");
            }
            if(confirmed){
              self.deleteFrame(index);
            }
          });
        }
        img.click(function(event){
          var t = event.target;
          self.setFrame(parseInt(t.id.replace("thumbnails-","")));
          $("#thumbnails img").removeClass("current");
          $(t).addClass("current");
          self.repaint();
        });
      });
      self.load(this.frames[this.currentIndex], true);
    },
    animateFiguresAlongPoints: function(figuresAndPoints, duration, callback){
      var transforms = [];
      figuresAndPoints.forEach(function(fp){
        var points = fp.points;
        var figure = fp.figure;

        var lastPoint = points[0];
        figure.transform(DIAGRAMO.Matrix.translationMatrix(lastPoint.x - figure.getMiddle().x, lastPoint.y - figure.getMiddle().y));
        points.forEach(function(point, index){
          if(point == lastPoint){
            return;
          }
          if(!transforms[index - 1]){
            transforms.push([]);
          }
          transforms[index - 1].push({
            type: "translate",
            dx: point.x - lastPoint.x,
            dy: point.y - lastPoint.y,
            preTransformMatrix: function(){
              return DIAGRAMO.Matrix.IDENTITY;//DIAGRAMO.Matrix.translationMatrix(-middle.x, -middle.y);
            },
            postTransformMatrix: function(){
              return DIAGRAMO.Matrix.IDENTITY;
            }
          })
          lastPoint = point;
        });
      });

      if(duration > DIAGRAMO.FPS){
        while(duration / transforms.length < DIAGRAMO.FPS){
          //find the smallest pair of transformations and merge.
          var sorted = _.sortBy(transforms, function(ts){
            var total = 0;
            var i = transforms.indexOf(ts)
            var nts = i < transforms.length - 2 ? transforms[i + 1] : transforms[i - 1];
            ts.forEach(function(t, index){
              var nt = nts[index];
              var _t =  (Math.abs(t.dx) + Math.abs(t.dy)) / 2;
              var _nt = (Math.abs(nt.dx) + Math.abs(nt.dy)) / 2;
              total += (_t + _nt) / 2
            })
            return total / ts.length;
          });
          var mergeIndex = transforms.indexOf(sorted[0]);
          if(mergeIndex < transforms.length - 2){
            transforms[mergeIndex + 1].forEach(function(t, index){
              t.dx += transforms[mergeIndex][index].dx;
              t.dy += transforms[mergeIndex][index].dy;
            });
          }
          else{
            transforms[mergeIndex - 1].forEach(function(t, index){
              t.dx += transforms[mergeIndex][index].dx;
              t.dy += transforms[mergeIndex][index].dy;
            });
          }
          transforms.splice(mergeIndex, 1);
        }
      }
      this.animateSequence2(figuresAndPoints.map(function(fp){return fp.figure}), transforms, duration, callback);
    },
    animateSequence2: function(figures, transfoms, duration, finishCallback){
      var perTransformDuration = (duration / transfoms.length);
      var i = 0;
      var self = this;
      var start = new Date().getTime();
      var callback = function(){
        var doTime = 0;
        if(i < transfoms.length - 1){
          self.animateSequenceFrame2(figures, transfoms[i++], perTransformDuration, callback);
        }
        else{
          self.animateSequenceFrame2(figures, transfoms[i++], perTransformDuration, finishCallback);
        }
      }
      callback();
    },

    animateSequenceFrame2: function(figures, transforms, duration, callback){
      if(transforms[0].type == "translate"){
        this.animateTranslations(figures, transforms, duration, callback);
      }
    },
    animateTranslations: function(figures, frames , duration, callback){
      if(!duration || duration < 10){
        figures.forEach(function(figure, index){
          figure.transform(DIAGRAMO.Matrix.translationMatrix(frames[index].dx, frames[index].dy));
        })
        callback();
      }
      else{
        var tmpMatrices = [];
        var factor = duration / (1000 / DIAGRAMO.FPS);
        var rounded = Math.floor(factor);
        delay = duration * (factor - rounded);// * rounded
        figures.forEach(function(figure, index){
          var tmpMatrix = DIAGRAMO.Matrix.translationMatrix(frames[index].dx, frames[index].dy);
          var tdx = tmpMatrix[0][2] / rounded;
          var tdy = tmpMatrix[1][2] / rounded;
          tmpMatrix[0][2] = Math.abs(frames[index].dx) < Math.abs(tdx) ? frames[index].dx : tdx;
          tmpMatrix[1][2] = Math.abs(frames[index].dy) < Math.abs(tdy) ? frames[index].dy : tdy;
          tmpMatrices.push(tmpMatrix);
        })
        this.animate2(figures, null, tmpMatrices, null, duration, 0, callback);
      }
    },
    animate2: function(figures, preTransformMatrix, matrices, postTransformMatrix, duration, delay, callback){
      var count = 0;
      var self = this;
      var transforming = false;
      var iter = 0;
      var func = function(){
        if(iter % 10 == 0){
          figures.forEach(function(figure, index){
            figure.transform(matrices[index]);
          });
        }
        iter++;
        if(new Date().getTime() - start >= duration){
          window.clearInterval(interval);
          if(callback){
            callback();
          }
        }
      };
      var start = new Date().getTime();
      var interval = window.setInterval(func, (1000 / DIAGRAMO.FPS) / 10) ;
    },

    animateAlongPointBasedPrimitive: function(figure, pbp, duration, callback){
      var points = pbp.getPoints();
      var _points = [];
      points.forEach(function(p){
        _points.push(p.clone());
      })
      points = _points;
      var transforms = [];
      var lastPoint = points[0];
      figure.transform(DIAGRAMO.Matrix.translationMatrix(lastPoint.x - figure.getMiddle().x, lastPoint.y - figure.getMiddle().y));
      points.forEach(function(point){
        if(point == lastPoint){
          return;
        }
        transforms.push({
          type: "translate",
          dx: point.x - lastPoint.x,
          dy: point.y - lastPoint.y,
          preTransformMatrix: function(){
            return DIAGRAMO.Matrix.IDENTITY;//DIAGRAMO.Matrix.translationMatrix(-middle.x, -middle.y);
          },
          postTransformMatrix: function(){
            return DIAGRAMO.Matrix.IDENTITY;
          }
        })
        lastPoint = point;
      });
      if(duration > DIAGRAMO.FPS){
        while(duration / transforms.length < DIAGRAMO.FPS){
          //find the smallest pair of transformations and merge.
          var sorted = _.sortBy(transforms, function(t){
            var i = transforms.indexOf(t)
            var _t =  (Math.abs(t.dx) + Math.abs(t.dy)) / 2;
            var nt = i < transforms.length - 2 ? transforms[i + 1] : transforms[i - 1];
            var _nt = (Math.abs(nt.dx) + Math.abs(nt.dy)) / 2;
            return (_t + _nt) / 2;
          });
          var mergeIndex = transforms.indexOf(sorted[0]);
          if(mergeIndex < transforms.length - 2){
            transforms[mergeIndex + 1].dx += transforms[mergeIndex].dx;
            transforms[mergeIndex + 1].dy += transforms[mergeIndex].dy;
          }
          else{
            transforms[mergeIndex - 1].dx += transforms[mergeIndex].dx;
            transforms[mergeIndex - 1].dy += transforms[mergeIndex].dy;
          }
          transforms.splice(mergeIndex, 1);
        }
      }
      this.animateSequence(figure, transforms, duration, callback);
    },

    animateSequenceFrame: function(figure, transform, duration, callback){
      if(transform.type == "translate"){
        this.animateTranslation(figure, transform.preTransformMatrix, transform.dx, transform.dy, transform.duration ? transform.duration : duration, transform.postTransformMatrix, callback);
      }
    },

    animateSequence: function(figure, sequence, duration, finishCallback){
      var start = new Date().getTime();
      this.repaint();
      var paintTime = new Date().getTime() - start;
      var perTransformDuration = (duration / sequence.length);
      var i = 0;
      var self = this;
      var start = new Date().getTime();
      var callback = function(){
        var doTime = 0;
        if(i < sequence.length - 1){
          self.animateSequenceFrame(figure, sequence[i], perTransformDuration, callback);
        }
        else{
          self.animateSequenceFrame(figure, sequence[i], perTransformDuration, finishCallback);
        }
        i++;
      }
      callback();
    },

    animateRotation: function(figure, preTransformMatrix, degrees, duration, postTransformMatrix, callback){
      if(!duration){
        figure.transform(DIAGRAMO.Matrix.rotationMatrix(degrees * (Math.PI / 180)));
      }
      else{
        var tmpMatrix = DIAGRAMO.Matrix.rotationMatrix((degrees / (duration / (1000 / DIAGRAMO.FPS))) * (Math.PI / 180));
        if(!postTransformMatrix){
          postTransformMatrix = DIAGRAMO.Matrix.clone(preTransformMatrix);
          postTransformMatrix[0][2] = - preTransformMatrix[0][2];
          postTransformMatrix[1][2] = - preTransformMatrix[1][2];
        }
        this.animate(figure, preTransformMatrix, tmpMatrix, postTransformMatrix, duration, callback);
      }
    },
    animateTranslation: function(figure, preTransformMatrix, dx, dy, duration, postTransformMatrix, callback){
      if(!duration){
        figure.transform(DIAGRAMO.Matrix.translationMatrix(dx, dy));
      }
      else{
        var tmpMatrix = DIAGRAMO.Matrix.translationMatrix(dx, dy);
        var factor = duration / (1000 / DIAGRAMO.FPS);
        var rounded = Math.floor(factor);
        delay = duration * (factor - rounded);// * rounded
        var tdx = tmpMatrix[0][2] / rounded;
        var tdy = tmpMatrix[1][2] / rounded;
        tmpMatrix[0][2] = Math.abs(dx) < Math.abs(tdx) ? dx : tdx;
        tmpMatrix[1][2] = Math.abs(dy) < Math.abs(tdy) ? dy : tdy;
        var count = 0;
        if(!postTransformMatrix){
          postTransformMatrix = DIAGRAMO.Matrix.clone(preTransformMatrix);
          postTransformMatrix[0][2] = - preTransformMatrix[0][2];
          postTransformMatrix[1][2] = - preTransformMatrix[1][2];
        }
        this.animate(figure, preTransformMatrix, tmpMatrix, postTransformMatrix, rounded, delay / 10, callback);
      }
    },
    animate: function(figure, preTransformMatrix, matrix, postTransformMatrix, calls, delay, callback){
      var count = 0;
      var self = this;
      var transforming = false;
      var iter = 0;
      var interval = window.setInterval(function(){
          if(_.isFunction(preTransformMatrix)){
            figure.transform(preTransformMatrix());
          }
          else{
            figure.transform(preTransformMatrix);
          }
          figure.transform(matrix);
          if(_.isFunction(postTransformMatrix)){
            figure.transform(postTransformMatrix());
          }
          else{
            figure.transform(postTransformMatrix);
          }
          self.repaint();
        transforming = false;
        count ++;//= (1000 / DIAGRAMO.FPS) / 10;
        if(count >= calls){
          window.clearInterval(interval);
          if(callback){
            if(delay > 3){
              window.setTimeout(function(){
                callback();
              }, delay - 2)
            }
            else{
              callback();
            }
          }
        }
      }, (1000 / DIAGRAMO.FPS));
    }
  });
};
